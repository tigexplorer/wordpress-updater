# wordpress-updater

A little script for updating wordpress.
It depends on wp-cli https://wp-cli.org/

## Install the script

 * Clone the repo `git clone https://gitlab.com/tigexplorer/wordpress-updater.git`
 * Change into dir `cd wordpress-updater`
 * Rename the config file `mv update.conf.template update.conf`
 * Edit the config file `nano update.conf`
 * And change the paths for your needs
 * Check the permissions of `update-wp.sh`
 * Check the permssions of `wordpress-updater` dir if you will writing the log here

## Running manually

 * `./update-wp.sh`

## Read the log

 * `less update.log`

## Be happy!
