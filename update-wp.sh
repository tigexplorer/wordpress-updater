#!/bin/bash
# Dieses kleine Script uebernimmt die Aktualisierung
# einer Wordpress-Installation
# Es setzt das Wordpress Kommando Tool voraus
#
# This script is for updating a wordpress install
# It depends on wp-cli https://wp-cli.org/

# Path and filename of config file
# For compatibility with cron jobs,
# notice here full path
UPDATE_CONFIG="update.conf"
# Path and filename of log file
# For compatibility with cron jobs,
# notice here full path instead of working dir and file name
UPDATE_LOG="$PWD/update.log"

# don't edit below this line
NOW=$(date +"%Y-%m-%d %H:%M:%S")
DATE=$(date +"%Y-%m-%d")

function f_check_logfile () {
	if [ ! -f $UPDATE_LOG ]
	then
		touch $UPDATE_LOG
	fi
}

function f_check_configfile () {
	if [ ! -f $UPDATE_CONFIG ]
	then
		echo "">> $UPDATE_LOG
		echo $NOW>> $UPDATE_LOG
		echo "Configfile not found:">> $UPDATE_LOG
		echo $UPDATE_CONFIG>> $UPDATE_LOG
		echo "Configfile not found.."
		echo "Let's lay down..."
		exit
	else
		source $UPDATE_CONFIG
		WP_DIR=$WP_DIR
		WP_DIR_BACKUP=$WP_DIR_BACKUP
		WP_DB_NAME=$WP_DB_NAME
		UPDATE_QUIET_MODE=$UPDATE_QUIET_MODE
	fi
}

echo ""
echo "Update wordpress..."
echo ""
echo "This script will backup"
echo "- your wordpress files"
echo "- your wordpress db"
echo "and then running the update for"
echo "- wordpress core"
echo "- wordpress plugins"
echo ""

f_check_logfile
f_check_configfile

if [ $UPDATE_QUIET_MODE = "n" ]; then
	read -p "Backup and update? (y/n) " -n 1
	echo ""
	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		echo ""
		echo "Update aborted"
		exit
	fi
fi

echo "">> $UPDATE_LOG
echo $NOW>> $UPDATE_LOG
echo "running...">> $UPDATE_LOG
echo "running..."
cd $WP_DIR
echo "check update..."
wp core check-update 2>&1 | tee -a $UPDATE_LOG

echo ""
if [ $UPDATE_QUIET_MODE = "n" ]; then
	read -p "Are you sure to proceed? (y/n) " -n 1
	echo ""
	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		echo ""
		echo "Update aborted"
		exit
	fi
fi

echo "export db..."
wp db export 2>&1 | tee -a $UPDATE_LOG

echo "move db to backup folder..."
mv "$WP_DB_NAME.sql" "$WP_DIR_BACKUP/$WP_DB_NAME.$DATE.sql" 2>&1 | tee -a $UPDATE_LOG

echo "compress dir..."
tar -vczf "$WP_DB_NAME.$DATE.gz" . 2>&1 | tee -a $UPDATE_LOG
echo "move compressed wp dir to backup folder"
mv "$WP_DB_NAME.$DATE.gz" "$WP_DIR_BACKUP/$WP_DB_NAME.$DATE.gz" 2>&1 | tee -a $UPDATE_LOG

echo "wp update..."
wp core update 2>&1 | tee -a $UPDATE_LOG
echo "wp plugin update all..."
wp plugin update --all 2>&1 | tee -a $UPDATE_LOG

echo "finish"
echo "finish">> $UPDATE_LOG
exit
